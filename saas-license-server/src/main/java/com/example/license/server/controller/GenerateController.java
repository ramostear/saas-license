package com.example.license.server.controller;


import com.example.license.common.domain.LicenseCheckModel;
import com.example.license.common.utils.os.AbstractServerInfo;
import com.example.license.common.utils.os.LinuxServerInfo;
import com.example.license.common.utils.os.WindowsServerInfo;
import com.example.license.server.domain.LicenseCreatorParam;
import com.example.license.server.utils.LicenseCreator;
import com.example.license.common.utils.ResultBean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * 用于生成证书文件，不能放在给客户部署的代码里
 */
@RestController
@RequestMapping("/license")
public class GenerateController {

    /**
     * 证书生成路径
     */
    @Value("${license.licensePath:/license.lic}")
    private String licensePath;

    /**
     * 密钥库存储路径
     */
    @Value("${license.privateKeysStorePath:/privateKeys.keystore}")
    private String  privateKeysStorePath;

    /**
     * 获取服务器硬件信息
     * @param osName 操作系统类型，如果为空则自动判断
     */
    @RequestMapping(value = "/getServerInfos",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResultBean<?> getServerInfos(@RequestParam(value = "osName",required = false) String osName) {
        // 操作系统类型
        if(StringUtils.isBlank(osName)){
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();
        AbstractServerInfo abstractServerInfo;
        //根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfo = new WindowsServerInfo();
        } else if (osName.startsWith("linux")) {
            abstractServerInfo = new LinuxServerInfo();
        }else{//其他服务器类型
            abstractServerInfo = new LinuxServerInfo();
        }
        return ResultBean.ok(LicenseCheckModel.getServerInfo(abstractServerInfo));
    }

    /**
     * 生成授权文件
     * @param param 生成授权需要的参数
     * {
     * 	"subject": "license_sub",
     * 	"privateAlias": "privateKey",
     * 	"keyPass": "abc@123",
     * 	"storePass": "abc@123",
     * 	"licensePath": "F:/keytool/license.lic",
     * 	"privateKeysStorePath": "F:/keytool/privateKeys.keystore",
     * 	"issuedTime": "2019-03-13 00:00:01",
     * 	"expiryTime": "2019-03-16 15:30:00",
     * 	"licenseCheckModel": {
     * 		"ipAddress": ["2001:0:2841:aa90:34fb:8e63:c5ce:e345", "192.168.153.155"],
     * 		"macAddress": ["00-00-00-00-00-00-00-E0","B0-52-16-27-F5-EF"],
     * 		"cpuSerial": "178BFBFF00660F51",
     * 		"mainBoardSerial": "L1HF7B400HZ"
     *        }
     * }
     *
     */
    @RequestMapping(value = "/generateLicense",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResultBean<?> generateLicense(@RequestBody LicenseCreatorParam param) {
        // 这里参数从配置中读取
        param.setLicensePath(licensePath);
        param.setPrivateKeysStorePath(privateKeysStorePath);

        LicenseCreator licenseCreator = new LicenseCreator(param);
        boolean result = licenseCreator.generateLicense();
        if (result) {
            return ResultBean.ok(param, "授权文件生成成功！");
        } else {
            return ResultBean.fail("授权文件生成失败！");
        }
    }

    /**
     * 下载文件到浏览器
     */
    @RequestMapping(value = "/download")
    public void downFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        File file = new File(licensePath);
        // 文件存在才下载
        if (file.exists()) {
            OutputStream out = null;
            FileInputStream in = null;
            try {
                // 读取要下载的内容
                in = new FileInputStream(file);
                // 解决文件名乱码问题，获取浏览器类型，转换对应文件名编码格式，IE要求文件名必须是utf-8, firefo要求是iso-8859-1编码
                String filename = URLEncoder.encode("license.lic", "UTF-8");
                // 设置一个响应头，无论是否被浏览器解析，都下载
                response.setHeader("Content-disposition", "attachment; filename=" + filename);
                // 将要下载的文件内容通过输出流写到浏览器
                out = response.getOutputStream();
                int len = 0;
                byte[] buffer = new byte[1024];
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            }
        }
    }
}