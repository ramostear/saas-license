package com.example.license.common.domain;

import com.example.license.common.utils.os.AbstractServerInfo;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义需要校验的License参数
 */
public class LicenseCheckModel implements Serializable {

    /**
     * 可被允许的IP地址
     */
    private List<String> ipAddress;

    /**
     * 可被允许的MAC地址
     */
    private List<String> macAddress;

    /**
     * 可被允许的CPU序列号
     */
    private String cpuSerial;

    /**
     * 可被允许的主板序列号
     */
    private String mainBoardSerial;

    public static LicenseCheckModel getServerInfo(AbstractServerInfo serverInfo){
        LicenseCheckModel result = new LicenseCheckModel();
        try {
            result.setIpAddress(serverInfo.getIpAddress());
            result.setMacAddress(serverInfo.getMacAddress());
            result.setCpuSerial(serverInfo.getCPUSerial());
            result.setMainBoardSerial(serverInfo.getMainBoardSerial());
        }catch (Exception e){
//            logger.error("获取服务器硬件信息失败",e);

        }
        return result;
    }

    public List<String> getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(List<String> ipAddress) {
        this.ipAddress = ipAddress;
    }

    public List<String> getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(List<String> macAddress) {
        this.macAddress = macAddress;
    }

    public String getCpuSerial() {
        return cpuSerial;
    }

    public void setCpuSerial(String cpuSerial) {
        this.cpuSerial = cpuSerial;
    }

    public String getMainBoardSerial() {
        return mainBoardSerial;
    }

    public void setMainBoardSerial(String mainBoardSerial) {
        this.mainBoardSerial = mainBoardSerial;
    }
}
